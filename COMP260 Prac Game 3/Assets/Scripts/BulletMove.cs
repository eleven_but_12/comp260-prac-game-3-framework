﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    // separate speed and direction so we can
    // tune the speed without changing the code
    public float speed = 15.0f;
    private float timerDestroy = 0;
    public Vector3 direction;
    private Rigidbody rigidbody;

    // Use this for initialization
    void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();
	}
	
    void FixedUpdate ()
    {
        rigidbody.velocity = speed * direction;
    }

    void OnCollisionEnter (Collision collision)
    {
        //Destroy the bullet
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        timerDestroy = timerDestroy + 1.0f * Time.deltaTime;

        if (timerDestroy > 3.0f)
        {
            Destroy(gameObject);
        }
	}
}
