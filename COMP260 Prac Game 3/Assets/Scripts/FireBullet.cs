﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour
{

    public BulletMove bulletPrefab;
    private float timerLoad = 0; //timer for bullet load

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
        {
            return;
        }


        //set timer to fire, minus 1
        timerLoad -= 1 * Time.deltaTime;

        if (Input.GetButtonDown("Fire1"))
        {
            if (timerLoad <= 0)
            {
                BulletMove bullet = Instantiate(bulletPrefab);
                // the bullet starts at the player's position
                bullet.transform.position = transform.position;

                // create a ray towards the mouse location
                Ray ray =
                    Camera.main.ScreenPointToRay(Input.mousePosition);
                bullet.direction = ray.direction;

                //set timer to fire to 1
                timerLoad = 1;
            }
            else
            {
                timerLoad -= 1;
            }

        }
    }
}